%define WRITE_SYSCODE 1
%define STDOUT 1       
%define EXIT_SYSCODE 60        

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
        mov rax, EXIT_SYSCODE
        syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax ; rax - total length of string. Set up zero to rax
.loop:
        cmp byte[rdi + rax], 0 ; if byte of [rdi + rax] is equal zero-symbol go to .end
        ; (rdi - address of first letter of string)
        je .end                 
        inc rax            ; else set up rax with rax + 1, go to .loop
        jmp .loop
.end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push rdi
        call string_length
        pop rsi
        mov rdx, rax            ; string length in bytes
        mov rax, WRITE_SYSCODE  ; 'write' syscall number
        mov rdi, STDOUT         ; stdout descriptor
        syscall
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
        mov rdi, '\n'
        
; Принимает код символа и выводит его в stdout
print_char:
        push rdi               ; rdi -> stack
        mov rax, WRITE_SYSCODE ; 'write' syscall number
        mov rdi, STDOUT        ; stdout descriptor
        mov rsi, rsp           ; take pointer of symbol code to write
        mov rdx, 1             ; 1 - count of bytes to write
        syscall
        pop rdi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
        test rdi, rdi
        jns print_uint          ; if number is unsigned, print it!
        push rdi                ; else store rdi to stack and
        mov rdi, `-`
        call print_char         ; print '-'
        pop rdi
        neg rdi
                
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.        
print_uint:
        sub rsp, 8
        mov byte [rsp], 0       ; allocate memory in stack
        mov r8, 1               ; count of bytes
        mov rax, rdi
        mov rcx, 10
.loop_devide:
        inc r8                  ; increment count of bytes
        xor rdx, rdx
        div rcx                 ; devision
        add rdx, '0'            ; to ASCII
        dec rsp                 
        mov [rsp], dl           ; store to stack
        test rax, rax           ; is the number 0?
        jnz .loop_devide
.print:
        mov rdi, rsp            ; pointer to first letter
        push r8                 ; store to stack r8 - count of bytes
        call print_string
        pop r8
        add rsp, r8
        add rsp, 7              ; restore rsp
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        xor rcx, rcx
.loop_on_string:
        mov r9b, [rdi + rcx]
        mov r10b, [rsi + rcx]    
        cmp r9b, r10b
        jnz .return_zero
        test r9b, r9b
        jz .return_one
        inc rcx
        jmp .loop_on_string
.return_one:
        mov rax, 1
        jmp .return_result
.return_zero:
        xor rax, rax
.return_result:
        ret 

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        sub rsp, 8
        xor rax, rax            ; 'read' syscall number
        xor rdi, rdi            ; stdin decriptor
        mov rsi, rsp            ; take pointer of symbol code to read
        mov rdx, 1              ; 1 - count of bytes to read
        syscall
        test rax, rax
        jle .stream_end
        mov al, [rsp]
.stream_end:
        add rsp, 8
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
        push rbp
        xor rax, rax
        xor rdx, rdx
        xor rbp, rbp
        push rdx
        push rsi
        push rdi
.readloop:
        call read_char
        test rax, rax           ; if we had taken the end of stream
        jz .return_success
        jl .return_fail
        cmp rax, ` `
        jz .skip_char
        cmp rax, `\t`
        jz .skip_char
        cmp rax, `\n`
        jz .skip_char        
.check_length:
        cmp rbp, [rsp + 8]
        jge .return_fail        
.store_char:
        pop rdi
        mov byte [rdi + rbp], al
        push rdi
        inc rbp
        jmp .readloop
.skip_char:
        test rbp, rbp
        jnz .return_fail
        jmp .readloop
.return_success:
        pop rdi
        mov byte [rdi + rbp], 0
        mov rax, rdi
        mov rdx, rbp
        jmp .popping_n_return
.return_fail:
        pop rdi
        mov rdx, rbp
        xor rax, rax
.popping_n_return:
        pop rsi
        pop r11
.return:
        pop rbp
        ret
                        
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        xor rcx, rcx
        xor r8, r8
        mov r9, 10
.loop:
        mov r8b, byte [rdi + rcx]
        cmp r8b, '0'
        jb .return
        cmp r8b, '9'
        ja .return
        sub r8b, '0'
        mul r9
        add rax, r8
        inc rcx
        jmp .loop
.return:
        mov rdx, rcx
        ret	

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        xor rax, rax
        xor rdx, rdx
        cmp byte [rdi], '-'
        jz .read_negative_num
        cmp byte [rdi], '+'
        jz .read_positive_num
        call parse_uint
        jmp .return_result
.read_positive_num:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .return_result
        inc rdx
        jmp .return_result
.read_negative_num:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .return_result
        inc rdx
        neg rax
.return_result:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        xor rax, rax
.loop:
        cmp rax, rdx
        jz .return_zero
        mov r9b, [rdi + rax]
        mov [rsi + rax], r9b
        inc rax
        test r9b, r9b
        jz .return
        jmp .loop
.return_zero:
        xor rax, rax
.return:
        ret
